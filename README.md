Engrave&#179; (EngraveCube - E&#179;)
===

Engrave&#179; (E&#179;) è un dispositivo autonomo portatile per l'incisione laser di superfici piane.

Il dispositivo non richiede l'ausilio di applicazioni esterne  (autonomo) ne nella fase di predisposizione del contenuto grafico da riprodurre, ne nella fase di esecuzione del lavoro.

La macchina fornisce un interfaccia utente web-based (accessibile da dispositivi mobili e fissi) che integra gli strumenti di:
- **athoring** attraverso cui comporre il contenuto grafico da riprodurre.
- **post-processing** attraverso cui convertire (in maniera silente) il contenuto grafico in codice macchina.
- **controll** attraverso cui controllare l'esecuzione della lavorazione.

Il software _embedded_ consente di caricare immagini, di  manipolarle e di arricchirle aggiungendo scritte ed emoji e di _pilotare_ l'esecuzione del lavoro di incisione in maniera rapida e semplice.

Le contenute dimensioni (soli 170x170x170mm), la possibilità di essere alimentato a batteria o da rete elettrica e la possibilità di connettersi a reti Wifi esistenti o di creare una propria rete HotSpot rendono E&#179; estremamente versatile e ne consentono l'utilizzo nei contesti più disparati.

L'area utile di incisione è di 50x50mm ed è possibile lavorare supporti (piastrine in legno, pcv, plexiglass, ecc) appositamente predisposti (attraverso un _cassetto adattatore_) oppure generiche superfici piane (ad esempio tavoli, cover di dispositvi mobili e pc portatili, ecc,... purchè di materiale compatibile) su cui andremo a posizionare direttamente la macchina.

E&#179; lavora modulando e direzionando un raggio laser da xx mW attraverso un gioco di specchi mossi da motori passo-passo. Tale architettura, con pochissime parti in movimento, consente alte velocità di lavorazione a fronte di consumi estremamente contenuti.

E&#179; rientra nel filone _17&#179; - machine_, dispositivi autonomi e compatti per il _digital manufacturing_ sviluppati dal Fablab Napoli. Tra questi il Fablab Napoli ha presentato alla Maker Fire Rome 2016, la macchina Tweet-A-Ball pensata per decorare palline da ping pong e premiata da Intel come miglior progetto nell'ambito del contest "Intel Makers" tra Fablabs italiani.
