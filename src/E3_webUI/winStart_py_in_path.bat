@echo off

REM Specifiing Flask application.
set FLASK_APP=E3-webUI.py

REM Enablnig developement option.
set FLASK_ENV=development

REM Starting web application
start "Press ctrl+pause to stop server." python -m flask run --host=0.0.0.0
