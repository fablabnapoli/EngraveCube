import os

class Config(object):
            SECRET_KEY = os.environ.get('SECRET_KEY') or 'Fablap2014'
            UPLOAD_FOLDER = '/path/to/the/uploads'
            ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
            SERIAL_PORT = 'COM23'
            SERIAL_BOUD = 9600
