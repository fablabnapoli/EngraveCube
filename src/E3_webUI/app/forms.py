from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from werkzeug.utils import secure_filename
from wtforms import StringField, SubmitField, BooleanField ,validators
from wtforms.validators import DataRequired

# Pay attention, your form MUST extend FlaskForm
class EngraveForm(FlaskForm):
    imageFile = FileField('Image to engrave', validators=[
        FileRequired(),
        FileAllowed(['png', 'jpg', 'jpeg'], 'Images only!')
    ])
    travelSpeed = StringField('Travel speed', validators=[DataRequired()])
    engraveSpeed = StringField('Engrave speed', validators=[DataRequired()])
    mirror = BooleanField('Mirror')
    flip = BooleanField('Flip')
    whiteValue = StringField('White threshold', validators=[DataRequired()])
    submit = SubmitField('Engrave')
