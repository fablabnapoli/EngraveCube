Note sull'uso di xburn
===

# Dipendenze
- Pillow-PIL
- Numpy

# Esecuzione dello script

`./cli.py filename.jpg 100 -o filename -p`

`./cli.py test 100 -tp -o testpattern -p`

Comando per l'esecuzione del PostProcesso `E3_cly.py`:

  `python E3_cli.py FBN.jpg 100 -o FBN -sr 1000 -br 30 -lp 200 -hp 255`

* 100: resize in px dell'immagine
* -o: nome del file di output con estenzione gcode
* -sr: Skip Rate - velocità di travel (quando non incide)
* -br: Nurn Rate - velocità di lavorazione (laser acceso)
* -lp: Minima potenza del laser per l'incisione.
* -hp: Massima potenza del laser per incisione.

Le variazzioni di grigio saranno ottenute variando la potenza del laser tra lp e hp.

# Rapporto dimensioni px, densità, dimensioni reali.
Un immagine di 300x300px a 96dpi avrà dimensione reale di 79.38x79.38mm.
1. 300px a 96dpi equivalgono a 300/96=3.125inch;
-  1inch = 25.4mm per cui 3.125*25.4 equivalgono a circa 79.38mm

I _dpi_ non sono una proprità fisica dell'immagine ma un metadato (che non inficia sulla qualità) che esprime il numero di punti per pollice da visualizzare o stampare. Ovvero esprime lo zoom in fase di visualizzazione/stampa.

I dpi indicano il numero di punti (pixels) da rappresentare per unità di spazio (Inch o mm). Ad esempio:

- 96dpi --> 96 px/inch --> 96/25.4 = 3.8 px/mm
- 72dpi --> 72 px/inch --> 72/25.4 = 2.8 px/mm 





# TDL
- [x] `E3ImageEditor ResizeKeepingAspectRatio` non consente di ingrandire le immagini.
- [x] Separare le classi `E3ImageEditor` `E3PostProcessor`
- Indagare il discorso delle Palette
- [x] Convertire i valori da intensità di pixel a laserPower
- [ ] Convertire gli spostamenti X,Y da pixel a mm
- [ ] Accorpare travel line successive. Se la curGcodeLine e la prevGcodeline sono entrambe travel rimpiazzo la prevGcodeline con la curGcodeLine nell'elenco delle GcodeLines.
- [ ] Spostare i commenti delle delle image line in coda alla Travel line.
- [ ] Spostare le classi del Post Processo nell'apposita cartella.
- [ ] Creare una CLI per eseguire il post-processing da shell.
- [ ] Spostare le righe di inizializzazione del Gcode (es G28, G90, G91 ecc) in una variabile.
- [ ] Prevedere delle linee di finalizzazione del Gcode.
