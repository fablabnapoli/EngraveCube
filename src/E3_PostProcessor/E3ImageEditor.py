#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import math
from PIL import Image, ImageOps
from itertools import groupby



class E3ImageEditor(object):
    """docstring for e3Image."""

    # The pointer to the image object
    img = None

    def __init__(self, imgPath):
        try:
            self.img = Image.open(imgPath)
        except IOError as e:
            print "E3-ImageEditor: File " + imgPath + " couldn't be open."
            sys.exit()

    # End __init__

    def ResizeKeepingAspectRatio(self, finalWith):
        finalHeight = (finalWith*self.img.size[1])/ \
        self.img.size[0]
        # self.img = self.img.resize((finalWith, finalHeight), Image.LANCZOS)
        self.img = self.img.resize((finalWith, finalHeight))

        # Perchè cli calcola la dimensione cosi?
        # resize = (
        #     int(math.floor(args.density * args.width)),
        #     int(math.floor(args.density * height))
        #     )
    # End ResizeKeepingAspectRatio

    def ResizeKeepingAspectRatioMM(self, finalWithMM):
        dpi = (96,96)
        # if self.img.info[dpi]:
            # dpi =  self.img.info[dpi]

        finalWith = (finalWithMM * dpi[0])/25.4
        finalHeight = (finalWith * self.img.size[1])/ \
        self.img.size[0]

        self.img = self.img.resize((int(math.floor(finalWith)),  int(math.floor(finalHeight))))
    # End ResizeKeepingAspectRatioMM


    def ConvertTo8BitBW(self):
        self.img = self.img.convert("L")
        pass
    # End ConvertTo8BitBW

    def getImage(self):
        return self.img
        pass
    # End getImage

    ## Flip image horizontally (left to right).
    #
    def mirror(self):
        self.img = ImageOps.mirror(self.img)
    # End mirror

    ## Flip image horizontally (left to right).
    #
    def flip(self):
        self.img = ImageOps.flip(self.img)
    # End flip
