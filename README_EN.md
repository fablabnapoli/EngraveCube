Engrave&#179; (EngraveCube)
===

Engrave Cube (E³) is the device designed by Fablab Naples that lets you create gorgeous laser engraving in a simple and safe manner. Through the embedded web control interface you can, directly from your smartphone:

compose the subject to be engraved by uploading images and adding text and emoji.
controll the engraving process.
E³ can be used to decorate flat surfaces such as tables, smartphone and pc covers, etc. or specially designed supports (50x50mm plates).
